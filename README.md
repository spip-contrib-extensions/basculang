# BascuLang

Un outil pour basculer entre modèles de multilinguisme.

## Contexte

SPIP fournit 2 modèles classiques de multilinguisme :
* par **secteurs de langue** : à la racine du site, on créé une rubrique par langue (la pluspart du temps nommées en majuscule selon le code de langue : FR, EN, ES, DE) et les sous-rubriques et articles héritent de la langue du secteur
* par **`<multi>`** : seuls les articles ont explictement une langue, les rubriques et autres objets sans liens de traduction sont écrit en `<multi>[fr]français[en]english</multi>`

De plus, SPIP permet d'établir des liens de traduction entre articles, ce qui permet de naviguer entre les diverses traduction d'un article.

## Prérequis

Pour que l'opération se passe bien, vous **devez** avoir un site massivement traduit (c-à-d, avec l'essentiel des articles traduits dans les différentes langues avec les bons liens de traduction entre eux)

De plus, voici les réglages à avoir dans `ecrire/?exec=configurer_multilinguisme` et plugin nécessaire.

### Passage de `<multi>` à secteurs de langues

- [ ] Rubriques : activer le menu de langue
- [x] Articles : activer le menu de langue
- [x] Articles : gérer les liens de traduction
- Plugin [Traduction de rubriques](https://contrib.spip.net/4737)

### Passage de secteurs de langues à `<multi>`

- [x] Rubriques : activer le menu de langue
- [x] ... seulement pour les rubriques situées à la racine ?
- [ ] Articles : activer le menu de langue
- [ ] Articles : gérer les liens de traduction

## Fonctionnement

1. Menu Maintenance - BascuLang
2. Le plugin détecte automatiquement votre configuration de multilinguisme et vous propose de basculer vers l'autre (l'opération est totalement réversible si votre site est totalement traduit ; à l'exception des sites référencés qui resteront dans le secteurs de la langue principale du site)

Notez bien : si votre site n'est pas entièrement traduit, les rubriques et articles sans traduction devront être déplacés manuellement à la fin du processus.

## Avertissements !

- le plugin n'a pas été testé sur une grosse base de données et nous ignorons comment il se comporte en cas de timeout
- faites une sauvegarde de votre base de données *avant* de lancer le processus afin de pouvoir revenir en arrière