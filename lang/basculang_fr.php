<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'basculang_titre' => 'BascuLang',
	'bascule_effectuee' => 'Bascule effectuée',
	'basculer_multi' => 'Basculer en <multi>',
	'basculer_secteurs' => 'Basculer en Secteurs de langue',

	// E
	'explication_basculang' => 'Changer le modèle de Multilinguisme du site en 1 clic',

	// M
	'modele_multilinguisme' => 'Votre modèle de multi-linguisme actuel est <strong>@modele@</strong>.'
);
