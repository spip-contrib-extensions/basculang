<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_basculang_charger_dist()
{
	$valeurs = array();
	$valeurs['model'] = "inconnu (il peut manquer le <a href='https://contrib.spip.net/Traduction-des-rubriques'>plugin TradRub</a>)";

	if (lire_config('multi_secteurs') == 'oui' && defined('_DIR_PLUGIN_TRADRUB')) {
		$valeurs['model'] = "par Secteurs de langue";
		$valeurs['basculang'] = "secteurs2multi";
		$valeurs['changement'] = _T('basculang:basculer_multi');
	}
	if (lire_config('multi_secteurs') == 'non') {
		$valeurs['model'] = "par <multi>";
		$valeurs['basculang'] = "multi2secteurs";
		$valeurs['changement'] = _T('basculang:basculer_secteurs');
	}


	return $valeurs;
}

function formulaires_basculang_verifier_dist()
{
	$erreurs = array();
	return $erreurs;
}

function formulaires_basculang_traiter_dist()
{
	$res = array();
	if (_request('basculang') == 'secteurs2multi') {
		basculang_secteurs2multi();
		$res['message_ok'] = _T('basculang:bascule_effectuee');
	}
	// verif la presence de tradrub
	if (_request('basculang') == 'multi2secteurs' && defined('_DIR_PLUGIN_TRADRUB')) {
		basculang_multi2secteurs();
		$res['message_ok'] = _T('basculang:bascule_effectuee');
	}
	return $res;
}

function basculang_secteurs2multi()
{
	include_spip('base/objets');
	include_spip('inc/autoriser');
	include_spip('action/editer_objet');

	$languePrincipaleSite = lire_config('langue_site');
	$ListeDesLangActive = basculang_ListeDesLangActive($languePrincipaleSite);
	
	$idSecteursLangues = array();
	// Fixer la langue des articles afin de permettre la transition inverse au mieux
	sql_update('spip_articles', array('langue_choisie' => sql_quote('oui')));

	// Traduire les rubriques en commaçant par les langue du site
	foreach ($ListeDesLangActive as $langue) {
		basculang_TraduireRubriques($langue['lang']);
	}

	// Rassembler les contenus en commençant par la langue principale du site
	foreach ($ListeDesLangActive as $langue) {
		basculang_RassemblerContenus($langue['lang']);
	}

	// Fusionner les rubriques récalcitrantres
	$select = array('id_rubrique', 'id_trad', 'lang');
	$from = 'spip_rubriques';
	$where = array('id_trad != id_rubrique', 'id_trad > 0');
	$ListeSsRubriques = sql_allfetsel($select, $from, $where);
	foreach ($ListeSsRubriques as $SsRubrique) {
		//echo 'SsRubrique<pre>';	print_r($SsRubrique);	echo '</pre>';
		basculang_DeplacerContenuRubrique($SsRubrique['id_rubrique'], $SsRubrique['id_trad'], $SsRubrique['lang']);
	}

	// Supprimer les sous-rubriques si vides
	// 1) Trouver les secteurs traduits
	$select = array('id_secteur');
	$from = 'spip_rubriques';
	$where = array('id_trad > 0', 'id_rubrique = id_secteur');
	$SecteursTraduits = sql_allfetsel($select, $from, $where);
	
	// 2) Faire le ménage des rubriques vides en commençant par les plus profondes
	foreach ($SecteursTraduits as $SecteurTraduit) {
		$select = array('id_rubrique');
		$from = 'spip_rubriques';
		$where = array('id_secteur = ' . $SecteurTraduit['id_secteur']);
		$orderby = 'profondeur DESC';
		$RubriquesSecteur = sql_allfetsel($select, $from, $where, '', $orderby);
		//echo 'RubriquesSecteur ' . $SecteurTraduit['id_secteur'] . '<pre>';	print_r($RubriquesSecteur);	echo '</pre>';
		
		foreach ($RubriquesSecteur as $RubriqueSecteur) {
			//echo "RubriqueSecteur['id_rubrique']:{$RubriqueSecteur['id_rubrique']}<br />";
			if (autoriser('supprimer', 'rubrique', $RubriqueSecteur['id_rubrique'])) {
			//echo "suppr:{$RubriqueSecteur['id_rubrique']}<br /><br />";
				$supprimer_rubrique = charger_fonction('supprimer_rubrique', 'action');
				$supprimer_rubrique($RubriqueSecteur['id_rubrique']);
			}
		}
	}

	// Déplacer les sous rubriques des secteurs de langue à la racine
	foreach ($SecteursTraduits as $SecteurTraduit) {
		$select = array('id_rubrique');
		$from = 'spip_rubriques';
		$where = array('id_secteur = ' . $SecteurTraduit['id_secteur'], 'profondeur = 1');
		$ListeSousRubriquesSecteurLanguePrincipale = sql_allfetsel($select, $from, $where);
		foreach ($ListeSousRubriquesSecteurLanguePrincipale as $SousRubriqueSecteurLanguePrincipale) {
			$objet = 'rubrique';
			$id_objet = $SousRubriqueSecteurLanguePrincipale['id_rubrique'];
			$set = array('id_parent' => 0);
			objet_modifier($objet, $id_objet, $set);
		}
		
		// Supprimer le secteur de langue si vide
		if (autoriser('supprimer', 'rubrique', $SecteurTraduit['id_secteur'])) {
			$supprimer_rubrique = charger_fonction('supprimer_rubrique', 'action');
			$supprimer_rubrique($SecteurTraduit['id_secteur']);
		}
	}


	// Changer la configuration du multilinguisme
	ecrire_meta('multi_secteurs', 'non');
	// Ne plus pouvoir choisir la langue des rubriques
	ecrire_meta('multi_objets', implode(',', array_diff(explode(',', lire_config('multi_objets')), array('spip_rubriques'))));
	// Mais pouvoir choisir la langue des articles
	ecrire_meta('multi_objets', implode(',', array_merge(explode(',', lire_config('multi_objets')), array('spip_articles'))));
	// Ajouter les articles aux liens de traduction
	ecrire_meta('gerer_trad_objets', implode(',', array_merge(explode(',', lire_config('gerer_trad_objets')), array('spip_articles'))));
}

function basculang_TraduireRubriques(string $langue) {
	// 1) Listes les articles originaux traduits 
	$select = array('id_article', 'id_rubrique');
	$from = 'spip_articles';
	$where = array("lang = '$langue'", 'id_trad = id_article');
	$ListeArticlesTraduits = sql_allfetsel($select, $from, $where);
	//echo 'ListeArticlesTraduits<pre>';	print_r($ListeArticlesTraduits);	echo '</pre>';
	
	// 2) Pour chaque article
	foreach ($ListeArticlesTraduits as $ArticleTraduit) {
		// a) trouver les 
		$select = array('id_article', 'id_rubrique');
		$from = 'spip_articles';
		$where = array('id_trad != id_article', 'id_trad = ' . $ArticleTraduit['id_article'] );
		$ListeArticlesTraductions = sql_allfetsel($select, $from, $where);
		//echo 'ListeArticlesTraductions<pre>';	print_r($ListeArticlesTraductions);	echo '</pre>';
		
		foreach ($ListeArticlesTraductions as $ArticlesTraduction) {
			basculang_TraduireRubrique($ArticleTraduit['id_rubrique'], $ArticlesTraduction['id_rubrique']);
		}

	}
}

function basculang_TraduireRubrique(int $rubriquePrincipale, int $rubriqueSecondaire) {
	// a) Changer le titre de la rubrique de la langue principale 
	// pour lui rajouter en <multi> le titre de la langue où se trouve l'article trouvé

	// Chercher le titre et la langue de la rubrique où se trouve l'article traduit
	$select = array('id_rubrique', 'titre', 'descriptif', 'texte', 'lang', 'id_trad', 'profondeur', 'id_parent');
	$from = 'spip_rubriques';
	$where = array('id_rubrique = ' . $rubriquePrincipale);
	$rubriqueLanguePrincipale = sql_fetsel($select, $from, $where);
	//echo 'rubriqueLanguePrincipale<pre>';	print_r($rubriqueLanguePrincipale);	echo '</pre>';// Chercher le titre et la langue de la rubrique où se trouve l'article traduit

	$select = array('id_rubrique', 'titre', 'descriptif', 'texte', 'lang', 'id_trad', 'profondeur', 'id_parent');
	$from = 'spip_rubriques';
	$where = array('id_rubrique = ' . $rubriqueSecondaire);
	$rubriquePasLanguePrincipale = sql_fetsel($select, $from, $where);
	//echo 'rubriquePasLanguePrincipale<pre>';	print_r($rubriquePasLanguePrincipale);	echo '</pre>';

	if (
		$rubriquePasLanguePrincipale
		and (
			$rubriqueLanguePrincipale['id_trad'] == 0
			or $rubriquePasLanguePrincipale['id_trad'] == 0
		)	
	) {
		//echo "{$rubriqueLanguePrincipale['id_rubrique']}:{$rubriqueLanguePrincipale['titre']}:{$rubriqueLanguePrincipale['id_trad']}:{$rubriqueLanguePrincipale['lang']} / {$rubriquePasLanguePrincipale['id_rubrique']}:{$rubriquePasLanguePrincipale['titre']}:{$rubriquePasLanguePrincipale['id_trad']}:{$rubriquePasLanguePrincipale['lang']} <br />";
		/*if ($rubriquePasLanguePrincipale['id_trad'] != 0) {
			$rubriqueLanguePrincipale['id_trad'] = $rubriquePasLanguePrincipale['id_trad'];
			$rubriquePasLanguePrincipale['id_trad'] = 0;
		}*/
		// Changement des champs des rubriques en multi
		$langLanguePrincipale = $rubriqueLanguePrincipale['lang'];
		$langPasLanguePrincipale = $rubriquePasLanguePrincipale['lang'];
		
		// Titre
		$titreLanguePrincipale = $rubriqueLanguePrincipale['titre'];
		$chaineslangues = array();
		$chaineslangues = basculang_ExtraireLanguesMulti($titreLanguePrincipale, $langLanguePrincipale, $chaineslangues);
		$titrePasLanguePrincipale = $rubriquePasLanguePrincipale['titre'];
		$chaineslangues = basculang_ExtraireLanguesMulti($titrePasLanguePrincipale, $langPasLanguePrincipale, $chaineslangues);
		$titremulti = basculang_BatirMulti(recuperer_numero($titreLanguePrincipale), $chaineslangues);
		
		// Descriptif
		$descriptifLanguePrincipale = $rubriqueLanguePrincipale['descriptif'];
		$chaineslangues = array();
		$chaineslangues = basculang_ExtraireLanguesMulti($descriptifLanguePrincipale, $langLanguePrincipale, $chaineslangues);
		$descriptifPasLanguePrincipale = $rubriquePasLanguePrincipale['descriptif'];
		$chaineslangues = basculang_ExtraireLanguesMulti($descriptifPasLanguePrincipale, $langPasLanguePrincipale, $chaineslangues);
		$descriptifmulti = basculang_BatirMulti(recuperer_numero($descriptifLanguePrincipale), $chaineslangues);
		
		// Texte
		$texteLanguePrincipale = $rubriqueLanguePrincipale['texte'];
		$chaineslangues = array();
		$chaineslangues = basculang_ExtraireLanguesMulti($texteLanguePrincipale, $langLanguePrincipale, $chaineslangues);
		$textePasLanguePrincipale = $rubriquePasLanguePrincipale['texte'];
		$chaineslangues = basculang_ExtraireLanguesMulti($textePasLanguePrincipale, $langPasLanguePrincipale, $chaineslangues);
		$textemulti = basculang_BatirMulti(recuperer_numero($texteLanguePrincipale), $chaineslangues);
		
		// Modifier le titre des rubriqueset rajouter le lien de traduction si nécessaire
		$objet = 'rubrique';
		$id_objet = $rubriqueLanguePrincipale['id_rubrique'];
		$set = array(
			// Gérer le cas où la rubrique secondaire est déjà une traduction
			'id_trad' => ($rubriquePasLanguePrincipale['id_trad'] != 0) ? $rubriquePasLanguePrincipale['id_trad'] : $rubriqueLanguePrincipale['id_rubrique'],
			'titre' => $titremulti,
			'descriptif' => $descriptifmulti,
			'texte' => $textemulti
		);
		objet_modifier($objet, $id_objet, $set);
		//echo "objet_modifier $id_objet<pre>";	print_r($set);	echo '</pre>';
		// Gérer le cas où la rubrique secondaire est déjà une traduction
		$id_objet = ($rubriquePasLanguePrincipale['id_trad'] != 0) ? $rubriquePasLanguePrincipale['id_trad'] : $rubriquePasLanguePrincipale['id_rubrique'];
		objet_modifier($objet, $id_objet, $set);
		//echo "objet_modifier $id_objet<pre>";	print_r($set);	echo '</pre>';
	}
	
	// Réccursion pour traduire et lier les rubriques parentes
	if ($rubriquePasLanguePrincipale and $rubriqueLanguePrincipale['profondeur'] > 0) {
		basculang_TraduireRubrique($rubriqueLanguePrincipale['id_parent'], $rubriquePasLanguePrincipale['id_parent']);
	}
}

function basculang_ExtraireLanguesMulti(string $multi, $lang, array $chaineslangues) {
	if (strpos($multi, '<multi>') === false) {
		$chaineslangues = array_unique(array_merge($chaineslangues, array(array('lang' => $lang, 'trad' => $multi))), SORT_REGULAR);
	} else {
		$listeLangues = basculang_ExtraireCodesLanguesMulti(supprimer_numero($multi), $lang);
	
		foreach ($listeLangues as $langue) {
			//echo 'chaineslangues 3)<pre>';	print_r($chaineslangues);	echo '</pre>';
			$chaineslangues = array_unique(array_merge($chaineslangues, array(array('lang' => $langue[1], 'trad' => extraire_multi($multi, $langue[1])))), SORT_REGULAR);
		}
	}
	return $chaineslangues;
}
function basculang_ExtraireCodesLanguesMulti(string $multi, string $lang) {
	$re = '/\[([a-z]+)\]/';

	preg_match_all($re, $multi, $langues, PREG_SET_ORDER, 0);
	//echo "langues 1:$multi/$lang<pre>";	print_r($langues);	echo '</pre>';
	if (count($langues) === 0) {
		$langues = array(array("$lang", $lang));
	}
	//echo "langues 2:$multi/$lang<pre>";	print_r($langues);	echo '</pre>';
	return $langues;
}

function basculang_BatirMulti(string $rang, array $chaineslangues) {
	$multi = '';
	foreach ($chaineslangues as $chainelangue) {
		if (strlen($chainelangue['trad']) > 0) {
			$multi .= '[' . $chainelangue['lang'] . ']' . supprimer_numero($chainelangue['trad']);
		}
	}
	
	if (strlen($multi) > 0) {
		$multi = (($rang !== '') ? $rang . '. ' : '') . '<multi>' . $multi . '</multi>';
		//echo 'titremulti<pre>';	echo htmlspecialchars($multi);	echo '</pre>';
	}

	return $multi;
}

function basculang_RassemblerContenus() {
	// 1) Récupérer les rubriques traduites originales en commençant par les plus profondes
	$select = array('id_rubrique');
	$from = 'spip_rubriques';
	$where = array('id_trad = id_rubrique');
	$orderby = 'profondeur DESC';
	$ListeRubriquesTraduites = sql_allfetsel($select, $from, $where, '', $orderby);
	//echo 'ListeRubriquesTraduites<pre>';	print_r($ListeRubriquesTraduites);	echo '</pre>';
	
	// 2) Pour chacunes, trouver les rubriques liées
	foreach ($ListeRubriquesTraduites as $RubriqueTraduite) {
		$select = array('id_rubrique', 'lang');
		$from = 'spip_rubriques';
		$where = array('id_trad = ' . $RubriqueTraduite['id_rubrique'], 'id_rubrique != ' . $RubriqueTraduite['id_rubrique']);
		$ListeRubriquesLiees = sql_allfetsel($select, $from, $where);
		//echo 'ListeRubriquesLiees ' . $RubriqueTraduite['id_rubrique'] . '<pre>';	print_r($ListeRubriquesLiees);	echo '</pre>'; 
		
		// 3) Pour chacunes, déplacer dedans tous les articles et sites dans la rubrique originale
		foreach ($ListeRubriquesLiees as $RubriqueLiee) {
			basculang_DeplacerContenuRubrique($RubriqueLiee['id_rubrique'], $RubriqueTraduite['id_rubrique'], $RubriqueLiee['lang']);
		}
	}
}

function basculang_DeplacerContenuRubrique(int $source, int $destination, string $lang) {
	// a) Trouver les articles
	$select = array('id_article', 'lang');
	$from = 'spip_articles';
	$where = array('id_rubrique = ' . $source);
	$ListeArticlesAdeplacer = sql_allfetsel($select, $from, $where);
	//echo 'ListeArticlesAdeplacer ' . $source . '<pre>';	print_r($ListeArticlesAdeplacer);	echo '</pre>'; 

	// b) Déplacer les articles
	foreach ($ListeArticlesAdeplacer as $ArticlesAdeplacer) {
		$objet = 'article';
		$id_objet = $ArticlesAdeplacer['id_article'];
		$set = array('id_parent' => $destination, 'lang' => $lang);
		objet_modifier($objet, $id_objet, $set);
	}

	// c) Tant qu'on y est, déplacer aussi tous les sites référencés de cette rubrique
	// dans la rubrique correspondante de la langue principale
	$select = array('id_syndic');
	$from = 'spip_syndic';
	$where = array('id_rubrique=' . $source);
	$ListeSitesPasLanguePrincipale = sql_allfetsel($select, $from, $where);
	foreach ($ListeSitesPasLanguePrincipale as $SitePasLanguePrincipale) {
		$set = array('id_parent' => $destination);
		objet_modifier('syndic', $SitePasLanguePrincipale['id_syndic'], $set);
	}
	
	// d) Tant qu'on y est, déplacer aussi toutes les sous-rubriques
	// dans la rubrique correspondante de la langue principale
	$select = array('id_rubrique');
	$from = 'spip_rubriques';
	$where = array('id_parent = ' . $source);
	$ListeSsRubriques = sql_allfetsel($select, $from, $where);
	//echo 'ListeSsRubriques ' . $source . '<pre>';	print_r($ListeSsRubriques);	echo '</pre>';
	foreach ($ListeSsRubriques as $SsRubrique) {
		$set = array('id_parent' => $destination);
		objet_modifier('rubrique', $SsRubrique['id_rubrique'], $set);
	}
}

function basculang_multi2secteurs()
{

	include_spip('base/objets');
	include_spip('inc/autoriser');
	include_spip('action/editer_objet');
	$languePrincipaleSite = lire_config('langue_site');
	$languePrincipaleSiteMaj = strtoupper($languePrincipaleSite);

	$ListeDesLangActive = basculang_ListeDesLangActive($languePrincipaleSite);	

	// Étape 1 : Création des secteurs de langues 
	foreach ($ListeDesLangActive as $LangActive) {
		// Est-ce que la rubrique existe déjà (ceinture et bretelles) ?
		$select = array('id_rubrique', 'lang');
		$from = 'spip_rubriques';
		$where = array("titre = '" . strtoupper($LangActive['lang']) . "'", "id_parent = 0");
		$RubriqueExistante = sql_fetsel($select, $from, $where);
		//echo 'RubriqueExistante<pre>';	print_r($RubriqueExistante);	echo '</pre>';
		

		if (!isset($RubriqueExistante)) {
			$objet = 'rubrique';
			$id_parent = 0;
			$id_rubrique = objet_inserer($objet, $id_parent);
			
			$objet = 'rubrique';
			$id_objet = $id_rubrique;
			// Gérer les liens de traduction entre rubriques
			if ($LangActive['lang'] === $languePrincipaleSite) {
				
				$set = array(
					'titre' => strtoupper($LangActive['lang']),
					'lang' => $LangActive['lang'],
					'id_trad' => $id_rubrique,
					'langue_choisie' => 'oui'
				);
				$id_trad_reference = $id_rubrique;
			} else {
				$set = array(
					'titre' => strtoupper($LangActive['lang']),
					'lang' => $LangActive['lang'],
					'id_trad' => $id_trad_reference,
					'langue_choisie' => 'oui'
				);
			}
			
			objet_modifier($objet, $id_objet, $set);
		} else {
			if ($RubriqueExistante['lang'] === $languePrincipaleSite) {
				$id_trad_reference = $RubriqueExistante['id_rubrique'];
			}
		}
	}

	// Étape 2 : Lister les rubriques à la racine avec titre multi pour les déplacer dans la langue principale ($id_trad_reference)
	$select = array('id_rubrique', 'titre', 'lang');
	$from = 'spip_rubriques';
	$where = array("titre LIKE '%<multi>%'", "id_parent = 0");
	$ListerubriqueMulti = sql_allfetsel($select, $from, $where);
	//echo 'ListerubriqueMulti<pre>';	print_r($ListerubriqueMulti);	echo '</pre>';

	foreach ($ListerubriqueMulti as $rubriqueMulti) {
		// Déplacer les rubrique Multi à l'interieur de la rubrique en langue principal du site.
		$objet = 'rubrique';
		$id_objet = $rubriqueMulti['id_rubrique'];
		$set = array('id_parent' => $id_trad_reference);
		objet_modifier($objet, $id_objet, $set);
	}

	// Étape 3 : éclater le contenu du secteur principal sur les autres langues
	// On va utiliser une fonction récursive pour parcourir l'arborescence de la langue principale

	basculang_EclaterContenuSecteurs($id_trad_reference);
	
	// Étape 4 : déplacer les rubriques restantes à la racine dans leur langue
	$select = array('id_rubrique', 'titre', 'lang');
	$from = 'spip_rubriques';
	$where = array('id_trad = 0', 'id_parent = 0', "langue_choisie = 'non'");
	$ListeRubriquesRacine = sql_allfetsel($select, $from, $where);
	//echo 'ListeRubriquesRacine<pre>';	print_r($ListeRubriquesRacine);	echo '</pre>';

	foreach ($ListeRubriquesRacine as $RubriqueRacine) {

		// Trouver la langue de la rubrique avec un de ses articles 
		$select = array('lang');
		$from = 'spip_articles';
		$where = array('id_rubrique = ' . $RubriqueRacine['id_rubrique']);
		$limit = '0,1';
		$RubriqueRacineLang = sql_fetsel($select, $from, $where, '', '', $limit);
		//echo 'RubriqueRacineLang<pre>';	print_r($RubriqueRacineLang);	echo '</pre>';
		
		if (isset($RubriqueRacineLang['lang'])) {
			$select = array('id_rubrique', 'titre', 'lang');
			$from = 'spip_rubriques';
			$where = array('id_trad != 0', "langue_choisie = 'oui'", 'id_parent = 0', "lang = '" . strtoupper($RubriqueRacineLang['lang'] . "'"));
			$RubriqueDestination = sql_fetsel($select, $from, $where);
			//echo 'RubriqueDestination<pre>';	print_r($RubriqueDestination);	echo '</pre>';
			
			if (isset($RubriqueDestination['id_rubrique'])) {
				// Déplacer les rubrique Multi à l'interieur de la rubrique en langue principal du site.
				$objet = 'rubrique';
				$id_objet = $RubriqueRacine['id_rubrique'];
				$set = array('id_parent' => $RubriqueDestination['id_rubrique']);
				objet_modifier($objet, $id_objet, $set);
			}
		}
	}

	// Final Changer la config en par secteur de langue
	// Activer le menu de langue sur les secteurs seulement
	ecrire_meta('multi_secteurs', 'oui');
	// Ne plus pouvoir choisir la langue des articles
	ecrire_meta('multi_objets', implode(',', array_diff(explode(',', lire_config('multi_objets')), array('spip_articles'))));
	// Mais pouvoir choisir la langue des secteurs
	ecrire_meta('multi_objets', implode(',', array_merge(explode(',', lire_config('multi_objets')), array('spip_rubriques'))));
	// Ajouter les rubriques aux liens de traduction
	ecrire_meta('gerer_trad_objets', implode(',', array_merge(explode(',', lire_config('gerer_trad_objets')), array('spip_rubriques'))));
	// Ajouter les articles aux liens de traduction
	ecrire_meta('gerer_trad_objets', implode(',', array_merge(explode(',', lire_config('gerer_trad_objets')), array('spip_articles'))));
}

function basculang_EclaterContenuSecteurs(int $id_parent)
{
	$languePrincipaleSite = lire_config('langue_site');

	// Liste les articles de la rubrique qui sont une traduction dans une langue étrangère à la langue principale
	$select = array('id_article', 'lang', 'id_rubrique');
	$from = 'spip_articles';
	$where = array("id_rubrique = $id_parent", "lang !='$languePrincipaleSite'");
	$ListeArticlesTraduits = sql_allfetsel($select, $from, $where);

	foreach ($ListeArticlesTraduits as $ArticleTraduit) {
		// Chercher la rubrique de destination
		$select = array('id_rubrique');
		$from = 'spip_rubriques';
		$where = array("lang = '" . $ArticleTraduit['lang'] . "'", "id_trad = " . $ArticleTraduit['id_rubrique']);
		$RubriqueDestination = sql_fetsel($select, $from, $where);

		// Elle n'existe pas, il faut la créer
		if (!isset($RubriqueDestination['id_rubrique'])) {
			// a) Chercher le titre de la rubrique mère de l'article
			//  et l'id de la rubrique parente de la rubrique mère
			$select = array('id_rubrique', 'titre', 'descriptif', 'texte', 'id_parent');
			$from = 'spip_rubriques';
			$where = array("id_rubrique = " .  $ArticleTraduit['id_rubrique']);
			$RubriqueMere = sql_fetsel($select, $from, $where);

			// b) Chercher l'id de la rubrique traduite dans laquelle on va créer la nouvelle rubrique
			$select = array('id_rubrique');
			$from = 'spip_rubriques';
			$where = array("id_trad = " .  $RubriqueMere['id_parent'], "lang = '" . $ArticleTraduit['lang'] . "'");
			$RubriqueDestinationMere = sql_fetsel($select, $from, $where);

			// c) Créer la rubrique
			$objet = 'rubrique';
			$id_parent_destination = $RubriqueDestinationMere['id_rubrique'];
			$Id_RubriqueDestination = objet_inserer($objet, $id_parent_destination);

			$objet = 'rubrique';
			$id_objet = $Id_RubriqueDestination;
			$set = array(
				'titre' => extraire_multi($RubriqueMere['titre'], $ArticleTraduit['lang']),
				'descriptif' => extraire_multi($RubriqueMere['descriptif'], $ArticleTraduit['lang']),
				'texte' => extraire_multi($RubriqueMere['texte'], $ArticleTraduit['lang']),
				'lang' => $ArticleTraduit['lang'],
				'id_trad' => $ArticleTraduit['id_rubrique'],
				'langue_choise' => 'oui'
			);
			objet_modifier($objet, $id_objet, $set);
		} else {
			$Id_RubriqueDestination = $RubriqueDestination['id_rubrique'];
		}
		// Déplacer l'article dans la rubrique de sa langue
		$objet = 'article';
		$id_objet = $ArticleTraduit['id_article'];
		$set = array('id_parent' => $Id_RubriqueDestination);
		objet_modifier($objet, $id_objet, $set);
	}

	// Lister les sous-rubriques de la rubrique en cours
	$select = array('id_rubrique');
	$from = 'spip_rubriques';
	$where = array("id_parent = $id_parent");
	$ListeSousRubriques = sql_allfetsel($select, $from, $where);

	foreach ($ListeSousRubriques as $SousRubrique) {
		basculang_EclaterContenuSecteurs($SousRubrique['id_rubrique']);
	}
}

function basculang_ListeDesLangActive(string $languePrincipaleSite) {
	// Lister les langues actives avec traduction (avec la langue principale en premier)
	$select = array('DISTINCT lang');
	$from = 'spip_articles';
	$where = array('id_trad > 0');
	$ListeDesLangActive = sql_allfetsel($select, $from, $where);	

	// Mettre la langue principale du site en premier
	$ListeDesLangActive = array_unique(array_merge(array(array('lang' => $languePrincipaleSite)), $ListeDesLangActive), SORT_REGULAR);
	//echo 'ListeDesLangActive<pre>';	print_r($ListeDesLangActive);	echo '</pre>';
	
	return $ListeDesLangActive;
}
